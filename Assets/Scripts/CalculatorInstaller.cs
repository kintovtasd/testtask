using UnityEngine;
using Zenject;

public class CalculatorInstaller : MonoInstaller
{      
    public override void InstallBindings()
    {
        PlayerPrefsGateway playerPrefsGateway = new PlayerPrefsGateway();
        CalculateUsecase calculateUsecase = new CalculateUsecase();
        GatewayInteractor gatewayInteractor = new GatewayInteractor(playerPrefsGateway);
        CalcPresenter calcPresenter = new CalcPresenter(calculateUsecase, gatewayInteractor);

        Container.Bind<ICalcPresenter>().To<CalcPresenter>().FromInstance(calcPresenter);
    }

}

