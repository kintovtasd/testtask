using UnityEngine;

public class PlayerPrefsGateway : IPlayerPrefsGateway
{
    public UserUIData GetUserUIData()
    {
        UserUIData data;
        data.expression = PlayerPrefs.HasKey("Data") ? PlayerPrefs.GetString("Data") : "";
        return data;
    }

    public void SetUserUIData(UserUIData data)
    {
        PlayerPrefs.SetString("Data", data.expression);
    }
}
