public interface IGatewayInteractor
{
    public void SaveData(UserUIData data);

    public UserUIData LoadData();
}
