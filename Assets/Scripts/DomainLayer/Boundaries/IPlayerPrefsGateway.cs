public interface IPlayerPrefsGateway
{ 
    public void SetUserUIData(UserUIData data);

    public UserUIData GetUserUIData();
}

