public class CalculateUsecase : ICalculateUsecase
{
    public string PerformAddition(UserUIData data)
    {
        string result;
        int operand1, operand2;
        string[] splittedString = data.expression.Split('+');
        if (splittedString.Length == 2 && int.TryParse(splittedString[0], out operand1) && int.TryParse(splittedString[1], out operand2))
            result = (operand1 + operand2).ToString();
        else result = "ERROR";
        return result;
    }
}
