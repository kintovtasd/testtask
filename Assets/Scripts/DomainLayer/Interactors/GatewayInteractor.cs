using Zenject;
public class GatewayInteractor : IGatewayInteractor
{
    
    IPlayerPrefsGateway gateway;

    public GatewayInteractor(IPlayerPrefsGateway gateway)
    {
        this.gateway = gateway;
    }
    public UserUIData LoadData()
    {
        return gateway.GetUserUIData();
    }

    public void SaveData(UserUIData data)
    {
        gateway.SetUserUIData(data);
    }
}
