using Zenject;
public class CalcPresenter : ICalcPresenter
{
    ICalculateUsecase calculateUsecase;
    IGatewayInteractor gatewayInteractor;

    public CalcPresenter(ICalculateUsecase calculateUsecase, IGatewayInteractor gatewayInteractor)
    {
        this.calculateUsecase = calculateUsecase;
        this.gatewayInteractor = gatewayInteractor;
    }
    public string Calculate(UserUIData data)
    {
        return calculateUsecase.PerformAddition(data);
    }

    public string LoadExpression()
    {
        return gatewayInteractor.LoadData().expression;
    }

    public void SaveExpression(UserUIData data)
    {
        gatewayInteractor.SaveData(data);
    }
}
