public interface ICalcPresenter
{
    public void SaveExpression(UserUIData data);
    public string LoadExpression();
    public string Calculate(UserUIData data);
}
