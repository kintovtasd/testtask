using UnityEngine;
using UnityEngine.UI;
using Zenject;
using TMPro;

public class CalculatorView : MonoBehaviour
{
    [Inject]
    ICalcPresenter presenter;
    UserUIData data;
    public TMP_InputField inputField;
    public Button button;
    void Start()
    {
        inputField.text = presenter.LoadExpression();

        inputField.onEndEdit.AddListener((string value) =>
        {
            data.expression = value;
            presenter.SaveExpression(data);
        });

        button.onClick.AddListener(() =>
        {
            data.expression = inputField.text;
            inputField.text = presenter.Calculate(data);
        });
    }
}
